﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public sealed class ControllerInput : MonoBehaviour
{

    static bool IsControllerDetected;
    public string ControllerType;
    public KeyCode ForwardButton;
    public KeyCode BackWardButton;
    public KeyCode RightButton;
    public KeyCode LeftButton;
    public KeyCode ActionButton;
    public KeyCode BoardButton;
    public KeyCode MenuButton;

    private string m_controllerType;

    #region SINGLETON PATTERN
    public static ControllerInput instance { get; private set; }
    private static readonly object padlock = new object();

    ControllerInput()
    {
    }

    public static ControllerInput Instance
    {
            get {

                lock (padlock)
            {
                if(instance == null)
                {
                    instance = GameObject.FindObjectOfType<ControllerInput>();
                    if (instance == null)
                    {
                        GameObject container = new GameObject("ControllerInput");
                        instance = container.AddComponent<ControllerInput>();
                        DontDestroyOnLoad(container);
                    }

                }
                return instance;
            }
        }
    }

    #endregion

    public void SetControllerType(string[] controllerNames)
    {
        if (controllerNames.Length != 0)
        {
            IsControllerDetected = true;
            switch (controllerNames[0].Length)
            {
                case 19:
                    ControllerType = "PS4";
                    break;
                case 33:
                    ControllerType = "XBOX";
                    break;
                default:
                    ControllerType = "Keyboard";
                    break;
            }
        }
        else
            ControllerType = "Keyboard";

        if (ControllerType != m_controllerType)
            SetControllerButton();
    }

    private void SetControllerButton()
    {
        if (ControllerType == "Keyboard")
        {
            ForwardButton  = KeyCode.Z;
            BackWardButton = KeyCode.S;
            RightButton    = KeyCode.D;
            LeftButton     = KeyCode.Q;
            ActionButton   = KeyCode.E;
            BoardButton    = KeyCode.Tab;
            MenuButton     = KeyCode.Escape;
        }
    }
}
