﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;


public class MainMenu : MonoBehaviour
{
    public Animator animator;
    public GameObject playerObject;
    public GameObject FadeImage;
    public GameObject parameterUI;
    public GameObject inputMapUI;

    public GameObject MainVolumeSlider;
    public GameObject MusicSlider;

    private bool isInInputMap;

    private void Awake()
    {
        ControllerInput.Instance.SetControllerType(Input.GetJoystickNames());
        GameData.Instance.LoadGame();

        MainVolumeSlider.GetComponent<Slider>().value = GameData.Instance.gameParameter.MainVolume;
        MusicSlider.GetComponent<Slider>().value = GameData.Instance.gameParameter.MusicVolume;

        playerObject.GetComponent<AudioSource>().volume = GameData.Instance.gameParameter.MainVolume;
    }

    private void Update()
    {

        if (Input.GetKeyDown(ControllerInput.Instance.MenuButton) && isInInputMap)
        {

            inputMapUI.SetActive(false);
            isInInputMap = false;
        }
    }

    public void StartGame()
    {
        animator.SetFloat("GameStarting", 1f);
        StartCoroutine(LoadScene());


    }

    IEnumerator LoadScene()
    {
        yield return null;

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("GameScene");
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        Debug.Log("Pro :" + asyncOperation.progress);
        //When the load is still in progress, output the Text and progress bar
        while (true)
        {
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.95f && !animator.IsInTransition(0))
            {
                animator.SetFloat("GameStarting", 0);
                StartCoroutine(GetComponent<StreamVideo>().PlayVideo(playerObject, FadeImage));
            }
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f && (!playerObject.GetComponent<VideoPlayer>().isPlaying && !GetComponent<StreamVideo>().coroutineHasStarted))
            {
                asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public void Parameter()
    {
        parameterUI.SetActive(true);

    }

    public void ShowInputMap()
    {
        inputMapUI.SetActive(true);
        isInInputMap = true;
    }

    public void CloseParameter()
    {
        parameterUI.SetActive(false);

        GameData.Instance.gameParameter.MainVolume = MainVolumeSlider.GetComponent<Slider>().value;
        GameData.Instance.gameParameter.MusicVolume = MusicSlider.GetComponent<Slider>().value;
        GameData.Instance.SaveGame();
    }
}
