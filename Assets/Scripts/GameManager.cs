﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class GameManager : MonoBehaviour
{
    public GameObject Board;
    public GameObject VideoPlayer;
    public GameObject VideoFade;
    public Texture LoseImage;
    public GameObject GameMenu;
    public PostIt PlantePostIt;
    public PostIt BaiePostIt;
    public GameObject descriptionWindow;
    public Sprite playerFace;
    public Image NPCFace;
    public GameObject parameterUI;

    public AudioClip normalMusic;
    public AudioClip boardMusic;
    public AudioClip loseMusic;

    private bool isInParameter;
    private DialogueManager m_dialogueManager;
    private int sentenceNumber;
    private string[] sentenceToType = new string[3];
    private int sentenceTyped; 
    private string m_suspect;
    private bool m_gameEnd;
    private bool m_playerIsTalking;

    public void OnEnable()
    {
        PlantePostIt.postItName = "Plante inconnu";
        BaiePostIt.postItName = "Baies dans un sac";
        m_dialogueManager = FindObjectOfType<DialogueManager>();

        foreach (string gameOjectTriggeredName in GameData.Instance.gameObjectTriggeredName)
        {
            GameObject.Find(gameOjectTriggeredName).GetComponent<InteractableObject>().hasBeenTriggered = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        float fps = 1f / Time.unscaledDeltaTime;
        DebugText.AddText("FPS", fps.ToString("N2"));


        DebugText.AddText("controller", ControllerInput.Instance.ControllerType);
        ControllerInput.Instance.SetControllerType(Input.GetJoystickNames());

        if (Input.GetKeyDown(ControllerInput.Instance.MenuButton) && !isInParameter)
            GameMenu.SetActive(!GameMenu.activeSelf);

        if (Input.GetKeyDown(ControllerInput.Instance.MenuButton) && isInParameter)
        {
            parameterUI.SetActive(false);
            isInParameter = false;
        }

        if (Input.GetKeyDown(ControllerInput.Instance.BoardButton))
        {
            Board.SetActive(!Board.activeSelf);
            if (Board.activeSelf)
            {
                float musicTime = GetComponent<AudioSource>().time;
                GetComponent<AudioSource>().clip = boardMusic;
                GetComponent<AudioSource>().Play();
                GetComponent<AudioSource>().time = musicTime;
            }
            else
            {
                float musicTime = GetComponent<AudioSource>().time;
                GetComponent<AudioSource>().clip = normalMusic;
                GetComponent<AudioSource>().Play();
                GetComponent<AudioSource>().time = musicTime;
            }
            GetComponent<PostItBoard>().isInBoard = !GetComponent<PostItBoard>().isInBoard;
            GetComponent<GenerateIdCardScroll>().GenerateIdCardScrollObject();
        }

        if (Input.GetMouseButtonDown(1) && GetComponent<PostItBoard>().isLinking)
        {
            GetComponent<PostItBoard>().DestroyLastRope();
        }
        else if(Input.GetMouseButtonDown(1) && !GetComponent<PostItBoard>().isLinking && GetComponent<PostItBoard>().linkedPostIt.Count > 0)
        {
            GetComponent<PostItBoard>().DestroyLastRope();
        }
        if (m_playerIsTalking && Input.GetKeyDown(ControllerInput.Instance.ActionButton) && sentenceTyped < sentenceNumber && m_dialogueManager.glassSprite.activeSelf)
        {
            StartCoroutine(m_dialogueManager.TypeDialogue(sentenceToType[sentenceTyped]));
            sentenceTyped++;
        }
       
        if(m_playerIsTalking && Input.GetKeyDown(ControllerInput.Instance.ActionButton) && sentenceTyped == sentenceNumber && m_dialogueManager.glassSprite.activeSelf)
        {
            m_playerIsTalking = false;
        }
        
        if(GetComponent<StreamVideo>().videoHasBeenPlayed && !GetComponent<StreamVideo>().videoPlayer.isPlaying)
        {
            SceneManager.LoadScene(0);
        }
        else if (m_gameEnd && Input.GetKeyDown(ControllerInput.Instance.ActionButton))
        {
            SceneManager.LoadScene(0);
        }

    }

    public void SaveGameData()
    {
        GameData.Instance.SaveGame();
    }

    public void LoadGameData()
    {
        GameData.Instance.LoadGame();
    }

    public void CheckIfEndGame() {
        m_suspect = GetComponent<GenerateIdCardScroll>().SuspectName;
        switch (m_suspect)
        {
            case "John M.":
                sentenceNumber = 3;
                sentenceToType[0] = "John savait que les candidats étaient présents ce jour là. Il a donc demandé à faire l'ouverture pour préparer le poison sans que personne le voit à la réserve. ";
                sentenceToType[1] = " Son torchon est la preuve de son crime, étant donné que les autres employés ont les leurs.";
                sentenceToType[2] = "John n'est pas pour l'égalité Omino Homme. De ce fait comme Bouc proclamait l'égalité dans sa politique et que la majorité était en sa faveur, il a du vouloir l'éliminer...";
                break;
            case "Mysmelle":
                sentenceNumber = 2;
                sentenceToType[0] = "Mysmelle avait des problèmes avec son mari...";
                sentenceToType[1] = " Elle rencontrait même Samantha, l'ami du concurrent en cachette, ce qui expliquerait tout.";
                break;
            case "Ritchy":
                sentenceNumber = 2;
                sentenceToType[0] = "Ritchy devait voir ses chances de gagner très faible face a Bouc...";
                sentenceToType[1] = "De plus entre leur prise de tête il a du imaginer ce stratagème pour le mettre sur le banc de touche le temps des élections...";
                break;
            case "Samantha":
                sentenceNumber = 2;
                sentenceToType[0] = "Samantha à du tomber amoureuse de Mysmelle...";
                sentenceToType[1] = "Cela expliquerait pourquoi elle connait la plante et qu'elle ai voulu l'empoisonner pour prendre la place de son mari...";
                break;
            default:
                break;
        }
        if(m_suspect == "John M.")
        {
            WinGame();
        }
        else
        {
            LoseGame();
        }
    }
    void WinGame()
    {
        VideoFade.GetComponent<Animator>().SetFloat("PlayAnimation", 1);

        m_playerIsTalking = true;
        StartCoroutine(checkAnimationState(true));
    }

    IEnumerator checkAnimationState(bool win)
    {
        
        while (VideoFade.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f && !VideoFade.GetComponent<Animator>().IsInTransition(0))
        {
            yield return null;
        }
        if(VideoFade.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f && !VideoFade.GetComponent<Animator>().IsInTransition(0) && m_playerIsTalking)
        {
            StartCoroutine(m_dialogueManager.TypeDialogue(sentenceToType[sentenceTyped]));
            NPCFace.sprite = playerFace;
            sentenceTyped++;
            descriptionWindow.SetActive(true);
            
        }
        while (VideoFade.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f && !VideoFade.GetComponent<Animator>().IsInTransition(0) && m_playerIsTalking)
        {
            VideoFade.GetComponent<Animator>().SetFloat("PlayAnimation", 0f);
            yield return null;
        }

        descriptionWindow.SetActive(false);

        if (win)
        {
            GetComponent<AudioSource>().volume = 0;
            StartCoroutine(GetComponent<StreamVideo>().PlayVideo(VideoPlayer, VideoFade));
        }
        else if (!win)
        {
            m_gameEnd = true;
            VideoFade.GetComponent<Animator>().SetFloat("PlayAnimation", 1f);
            VideoPlayer.SetActive(true);
            GetComponent<AudioSource>().clip = loseMusic;
            GetComponent<AudioSource>().Play();
            VideoPlayer.GetComponent<RawImage>().texture = LoseImage;
        }

    }

    void LoseGame()
    {
        
        VideoFade.GetComponent<Animator>().SetFloat("PlayAnimation", 1);
        m_playerIsTalking = true;

        StartCoroutine(checkAnimationState(false));
    }

    public void ReturnToTheGame()
    {
        GameMenu.SetActive(false);
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
    }

    public void Parameter()
    {
        parameterUI.SetActive(true);
        isInParameter = true;
    }

}
