﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour
{

    public RawImage rawImage;
    public VideoPlayer videoPlayer;
    public AudioSource audioSource;
    public bool coroutineHasStarted = true;
    public bool videoHasBeenPlayed;
    

    public IEnumerator PlayVideo(GameObject playerObject, GameObject fadeImage)
    {
        playerObject.SetActive(true);
        playerObject.GetComponent<RawImage>().color = new Color(playerObject.GetComponent<RawImage>().color.r, playerObject.GetComponent<RawImage>().color.g, playerObject.GetComponent<RawImage>().color.b, 0);
        videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);

        while (!videoPlayer.isPrepared)
        {
            yield return waitForSeconds;
        }
        fadeImage.SetActive(false);
        playerObject.GetComponent<RawImage>().color = new Color(playerObject.GetComponent<RawImage>().color.r, playerObject.GetComponent<RawImage>().color.g, playerObject.GetComponent<RawImage>().color.b, 1);
        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
        audioSource.Play();
        videoHasBeenPlayed = true;
        coroutineHasStarted = false;
    }
}
