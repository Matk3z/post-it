﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform m_player;
    public float cameraSpeed;
    CameraState m_Camera;
    class CameraState
    {
        public Vector3 offset;
        public float targetx, targety, targetz;


        public CameraState(Vector3 os)
        {
            offset = os;
        }
        public void UpdateCameraTransform(Transform target)
        {
            targetx = target.position.x + offset.x ;
            targety = target.position.y + offset.y;
            targetz = target.position.z + offset.z;

        }
        
        public Vector3 LerpTo(Vector3 position, float speed)
        {
            return Vector3.Lerp(position, new Vector3(targetx, targety, targetz), Time.deltaTime * speed);
 
        }

    }


    // Start is called before the first frame update
    void OnEnable()
    {
        m_player = FindObjectOfType<Player>().transform;
        m_Camera = new CameraState(transform.position - m_player.position);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        m_Camera.UpdateCameraTransform(m_player);
        var positionLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / 0.001f) * Time.fixedDeltaTime);
        positionLerpPct *= cameraSpeed;

        transform.position = m_Camera.LerpTo(transform.position, positionLerpPct);

    }
}
