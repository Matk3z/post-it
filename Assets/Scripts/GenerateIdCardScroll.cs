﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GenerateIdCardScroll : MonoBehaviour
{
    public Sprite[] IdCardSprites;
    public GameObject IdCardPrefab;
    public Transform IdCardParent;
    public GameObject idCardScroll;
    public List<GameObject> IdCards = new List<GameObject>();
    public GameObject designateButton;
    public string SuspectName;

    private bool canBeDesignated;
    private float IdCardParentSize;

    public void GenerateIdCardScrollObject()
    {

        foreach(GameObject IdCard in IdCards)
        {
            Destroy(IdCard);
        }
        IdCards = new List<GameObject>();
        designateButton.SetActive(false);
        canBeDesignated = false;
        foreach (Sprite IdCardSprite in IdCardSprites)
        {
            if (GameObject.Find(IdCardSprite.name) != null && GameObject.Find(IdCardSprite.name).GetComponentInChildren<NPC>().hasBeenTriggered)
            {
                GameObject IdCard = Instantiate(IdCardPrefab, IdCardParent);
                IdCard.name = $"IdCard : {IdCardSprite.name}";

                IdCard.GetComponent<Image>().sprite = IdCardSprite;
                IdCards.Add(IdCard);
                
            }
        }
        IdCardParentSize = (IdCards.Count * 200) + 50;

        int i = 0;

        foreach (GameObject IdCard in IdCards)
        {
            if (SuspectName == IdCard.name.Split(':')[1].Trim())
            {
                IdCardParentSize = (IdCards.Count * 200) + 150;
            }
        }
        IdCardParent.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, IdCardParentSize);
        foreach (GameObject IdCard in IdCards)
        {
            Vector3 newPostion = new Vector3(0, (IdCardParentSize / 2) - (1668 * 0.3f * 0.5f) + 100 - 200 * i - 100 * Convert.ToInt32(canBeDesignated), 0);
            IdCard.GetComponent<RectTransform>().anchoredPosition = newPostion;
            i++;
            if (SuspectName == IdCard.name.Split(':')[1].Trim())
            {
                designateButton.SetActive(true);
                designateButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(newPostion.x, newPostion.y - 105, newPostion.z);
                canBeDesignated = true;
            }

        }

    }
}
