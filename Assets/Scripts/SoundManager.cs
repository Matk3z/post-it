﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioSource musicAudioSource;

    public AudioClip[] audioClips;

    private void OnEnable()
    {
        audioSource.clip = audioClips[1];
        audioSource.Play();
        musicAudioSource.clip = audioClips[0];
        musicAudioSource.Play();
        StartCoroutine(FadeIn(musicAudioSource, 10, 0.4f));
        GetComponent<AudioSource>().volume = GameData.Instance.gameParameter.MainVolume;
        GetComponent<AudioSource>().volume = GameData.Instance.gameParameter.MusicVolume;
    }

    IEnumerator FadeIn(AudioSource audiosource, float fadeTime, float maxVolume)
    {
        audiosource.volume = 0;

        while (audiosource.volume < maxVolume)
        {
            audiosource.volume += 1 * maxVolume * Time.deltaTime / fadeTime;
            DebugText.AddText("Music Volume", audiosource.volume.ToString());
            yield return null;
        }

        print("Music has fade in");
    }

}

