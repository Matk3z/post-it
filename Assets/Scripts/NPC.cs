﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using LitJson;

public class NPC : InteractableObject
{
    public Dictionary<string, string[][]> dialogue = new Dictionary<string, string[][]>();
    public Dictionary<string, string> NPCAnswer = new Dictionary<string, string>();
    public Dictionary<string, string> postItToAdd = new Dictionary<string, string>();
    public Dictionary<string, string> postItUpdate = new Dictionary<string, string>();
    public GameObject gameManager;
    public Sprite NPCNameSprite;

    string endSentence;
    string startSentence;
    string NPCName;

    public DialogueManager dialogueManager;
    public GameObject IdcardUIPrefab;
    public GameObject canvas;

    // Start is called before the first frame update
    void Start()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
        CreateDict();
    }

    // Update is called once per frame
    void Update()
    {
        m_playerDistance = new Vector2(m_playerScript.transform.position.x - transform.position.x, m_playerScript.transform.position.z - transform.position.z);


        DebugText.AddText($"{gameObject.name}", (m_playerDistance.magnitude < 1f).ToString());
        if (!gameManager.GetComponent<PostItBoard>().isInBoard && m_playerDistance.magnitude < 1f && Input.GetKeyDown(ControllerInput.Instance.ActionButton) && !m_playerScript.isInteracting)
        {

            if (!hasBeenTriggered)
            {
                Instantiate(IdcardUIPrefab, canvas.transform);
            }
            m_playerScript.isInteracting = true;
            m_playerScript.isTalking = true;
            m_playerScript.GetComponent<Animator>().SetBool("IsTalking", true);
            transform.parent.GetComponent<Animator>().SetBool("IsTalking", true);
            hasBeenTriggered = true;

            if (!GameData.Instance.gameObjectTriggeredName.Contains(name))
            {
                print("OH YOW");
                GameData.Instance.gameObjectTriggeredName.Add(name);
            }

            InteractNPC();
        }
    }

    void CreateDict()
    {
        NPCName = gameObject.name;
        print(NPCName);
        DebugText.AddText("json path",Application.dataPath + "/JsonData/dialogue_fr.json");
        string dialogueString = File.ReadAllText(Application.dataPath + "/JsonData/dialogue_fr.json", System.Text.Encoding.GetEncoding("iso-8859-1"));
        JsonData dialogueData = JsonMapper.ToObject(dialogueString);
        string[] dialogueSentences = new string[dialogueData["Dialogue"][NPCName]["dialogueSentence"].Count];


        for (int i = 0; i< dialogueData["Dialogue"][NPCName]["dialogueSentence"].Count; i++)
        {
            dialogueSentences[i] = dialogueData["Dialogue"][NPCName]["dialogueSentence"][i].ToString();
            string[][] tempString2D = new string[dialogueData["Dialogue"][NPCName][dialogueSentences[i]].Count][];

            for (int x = 1; x <= dialogueData["Dialogue"][NPCName][dialogueSentences[i]].Count; x++)
            {
                string[] tempString1D = new string[dialogueData["Dialogue"][NPCName][dialogueSentences[i]][$"answer{x}"].Count];

                for (int y = 0; y < dialogueData["Dialogue"][NPCName][dialogueSentences[i]][$"answer{x}"].Count; y++)
                {

                    tempString1D[y] = dialogueData["Dialogue"][NPCName][dialogueSentences[i]][$"answer{x}"][y].ToString();
                }
                tempString2D[x - 1] = tempString1D;

            }
            dialogue[dialogueSentences[i]] = tempString2D;
        }


        string NPCAnswerString = File.ReadAllText(Application.dataPath + "/JsonData/NPCAnswer_fr.json", System.Text.Encoding.GetEncoding("iso-8859-1"));
        JsonData NPCAnswerData = JsonMapper.ToObject(NPCAnswerString);

        // parse json for AnswerData
        string[] NPCAnswerSentences = new string[NPCAnswerData["NPCAnswer"][NPCName]["answerSentence"].Count];
        for (int i = 0; i < NPCAnswerData["NPCAnswer"][NPCName]["answerSentence"].Count; i++)
        {
            NPCAnswerSentences[i] = NPCAnswerData["NPCAnswer"][NPCName]["answerSentence"][i].ToString();
        }
        foreach (string NPCAnswerSentence in NPCAnswerSentences)
        {
            NPCAnswer[NPCAnswerSentence] = NPCAnswerData["NPCAnswer"][NPCName][NPCAnswerSentence].ToString();
        }
        
        // Parse json for what post it to add for what sentence
        string[] postItToAddSentences = new string[NPCAnswerData["NPCAnswer"][NPCName]["postItToAddSentence"].Count];
        for (int i = 0; i < NPCAnswerData["NPCAnswer"][NPCName]["postItToAddSentence"].Count; i++)
        {
            postItToAddSentences[i] = NPCAnswerData["NPCAnswer"][NPCName]["postItToAddSentence"][i].ToString();

        }
        foreach(string postItToAddSentence in postItToAddSentences)
        {
            postItToAdd[postItToAddSentence] = NPCAnswerData["NPCAnswer"][NPCName][postItToAddSentence].ToString();
        }

        // Parse json for what post it needs to be updated for what line of dialogue
        string[] postItUpdateSentences = new string[NPCAnswerData["NPCAnswer"][NPCName]["postItUpdateSentence"].Count];
        for(int i = 0; i < NPCAnswerData["NPCAnswer"][NPCName]["postItUpdateSentence"].Count; i++)
        {
            postItUpdateSentences[i] = NPCAnswerData["NPCAnswer"][NPCName]["postItUpdateSentence"][i].ToString();
        }
        foreach(string postItUpdateSentence in postItUpdateSentences)
        {
            postItUpdate[postItUpdateSentence] = NPCAnswerData["NPCAnswer"][NPCName][postItUpdateSentence].ToString();
        }

        startSentence = NPCAnswerData["NPCAnswer"][NPCName]["startSentence"].ToString();
        endSentence = NPCAnswerData["NPCAnswer"][NPCName]["endSentence"].ToString();

    }


    public void InteractNPC()
    {


        base.InteractObject();

        if(m_playerDistance.magnitude < 1f)
        {
            dialogueManager.StartDialogue(gameObject, dialogue, startSentence, endSentence);
        }

    }

}

