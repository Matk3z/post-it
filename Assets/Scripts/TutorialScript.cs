﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScript : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(ControllerInput.Instance.ActionButton))
        {
            Destroy(gameObject);
        }
    }
}
