﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public GameObject[] answerPanels;
    public GameObject endPannel;
    public GameObject NPCPanel;
    public Player player;
    public GameObject glassSprite;
    public Image NPCNameUI;

    private string buttonText;
    private NPC npcScript;
    private PostItBoard postItBoard;
    private bool dialogueHasNoAnswer = false;
    private string m_firstSentence;
    private string m_endSentence;
    private int dialogueLenght;
    private bool isTyping;
    private bool dialogueStarting;


    public void UpdateDialogueButton(GameObject Button)
    {
        Button.GetComponent<UpdateSprite>().UpdateSpriteMouseNotHovering();
        postItBoard = FindObjectOfType<PostItBoard>();
        buttonText = Button.GetComponentInChildren<Text>().text;
        UpdateDialogue(npcScript.dialogue, npcScript.NPCAnswer, npcScript.postItToAdd, npcScript.postItUpdate);

    }

    public void UpdateDialogue(Dictionary<string, string[][]> dialogue, Dictionary<string, string> NPCAnswer, Dictionary<string, string> PostItToAdd, Dictionary<string,string> postItUpdate)
    {

        StartCoroutine(TypeDialogue(dialogue, NPCAnswer[buttonText]));

        foreach (var key in PostItToAdd.Keys)
        {
            if(key == NPCAnswer[buttonText])
            {

                if (postItUpdate.ContainsKey(PostItToAdd[NPCAnswer[buttonText]]))
                {
                    print(key);
                    postItBoard.UpdatePostIt(PostItToAdd[NPCAnswer[buttonText]], postItUpdate[PostItToAdd[NPCAnswer[buttonText]]]);
                    break;
                }

                postItBoard.AddpostIt(PostItToAdd[NPCAnswer[buttonText]]);
            }
        }


        foreach (GameObject answerUI in answerPanels)
            answerUI.SetActive(false);

        endPannel.SetActive(false);


    }

    public void Update()
    {
        if ((Input.GetKeyDown(ControllerInput.Instance.ActionButton) || Input.GetMouseButtonDown(0)) && isTyping && !dialogueStarting)
            isTyping = false;
        else if (dialogueHasNoAnswer && (Input.GetKeyDown(ControllerInput.Instance.ActionButton) || Input.GetMouseButtonDown(0)) && player.isInteracting && !isTyping)
            RestartDialogue(npcScript.dialogue);
    }

    public void StartDialogue(GameObject npc, Dictionary<string, string[][]> dialogue, string firstSentence, string endSentence)
    {
        dialogueStarting = true;
        
        npcScript = npc.GetComponent<NPC>();
        NPCNameUI.gameObject.SetActive(true);
        NPCNameUI.sprite = npcScript.NPCNameSprite;
        StartCoroutine(TypeDialogue(dialogue, firstSentence));

        dialogueHasNoAnswer = false;
        m_firstSentence = firstSentence;
        m_endSentence = endSentence;

        print($"starting dialog with {npcScript.gameObject.name}");

    }

    public void RestartDialogue(Dictionary<string, string[][]> dialogue)
    {
        if(npcScript.m_playerDistance.magnitude > 1f)
        {
            print(npcScript.m_playerDistance.magnitude);
            return;
        }
        glassSprite.SetActive(false);
        npcScript.descriptionPanel.GetComponentInChildren<Text>().text = m_firstSentence;
        print("Dialogue restarted !");

        ShowDialogueOption(dialogue, m_firstSentence);
    }
    public IEnumerator TypeDialogue(string sentence)
    {
        glassSprite.SetActive(false);
        int i = 0;
        Text NPCText = npcScript.descriptionPanel.GetComponentInChildren<Text>();
        NPCText.text = "";
        isTyping = true;
        while (i < sentence.Length)
        {
            NPCText.text += sentence[i];
            i++;
            yield return null;
        }
        isTyping = true;
        dialogueStarting = false;
        glassSprite.SetActive(true);
        NPCText.text = sentence;
    }
    public IEnumerator TypeDialogue(Dictionary<string, string[][]> dialogue, string sentence)
    {
        int i = 0;
        Text NPCText = npcScript.descriptionPanel.GetComponentInChildren<Text>();
        NPCText.text = "";
        isTyping = true;
        while (i < sentence.Length && isTyping)
        {
            NPCText.text += sentence[i];
            i++;
            yield return null;
        }
        isTyping = true;
        dialogueStarting = false;
        NPCText.text = sentence;
        ShowDialogueOption(dialogue, sentence);
    }

    private void ShowDialogueOption(Dictionary<string, string[][]> dialogue, string sentence)
    {
        dialogueLenght = 0;

        if (!dialogue.ContainsKey(sentence))
        {
            glassSprite.SetActive(true);
            dialogueHasNoAnswer = true;
            return;
        }

        for (int i = 0; i < dialogue[sentence].Length; i++)
        {
            if (dialogue[sentence][i][1] == "NoTrigger")
            {
                answerPanels[dialogueLenght].SetActive(true);
                answerPanels[dialogueLenght].GetComponentInChildren<Text>().text = dialogue[sentence][i][0];
                dialogueLenght += 1;
            }
            else if (GameObject.Find(dialogue[sentence][i][1]) != null && GameObject.Find(dialogue[sentence][i][1]).GetComponent<InteractableObject>().hasBeenTriggered && dialogue[sentence][i].Length == 2)
            {
                answerPanels[dialogueLenght].SetActive(true);
                answerPanels[dialogueLenght].GetComponentInChildren<Text>().text = dialogue[sentence][i][0];
                dialogueLenght += 1;
            }
            else if((dialogue[sentence][i].Length > 2) && ((GameObject.Find(dialogue[sentence][i][1]) != null && (GameObject.Find(dialogue[sentence][i][1]).GetComponent<InteractableObject>() != null && GameObject.Find(dialogue[sentence][i][1]).GetComponent<InteractableObject>().hasBeenTriggered) || (GameObject.Find(dialogue[sentence][i][1]).GetComponent<NPC>() != null && GameObject.Find(dialogue[sentence][i][1]).GetComponent<NPC>().hasBeenTriggered)) || (GameObject.Find(dialogue[sentence][i][2]) != null && ((GameObject.Find(dialogue[sentence][i][2]).GetComponent<InteractableObject>() != null && GameObject.Find(dialogue[sentence][i][2]).GetComponent<InteractableObject>().hasBeenTriggered) || (GameObject.Find(dialogue[sentence][i][2]).GetComponent<NPC>() != null && GameObject.Find(dialogue[sentence][i][2]).GetComponent<NPC>().hasBeenTriggered)))))
            {
                answerPanels[dialogueLenght].SetActive(true);
                answerPanels[dialogueLenght].GetComponentInChildren<Text>().text = dialogue[sentence][i][0];
                dialogueLenght += 1;
            }
        }
        print(dialogueLenght);
        endPannel.SetActive(true);
        endPannel.GetComponentInChildren<Text>().text = m_endSentence;
    }

    public void EndDialogue(GameObject button)
    {
        button.GetComponent<UpdateSprite>().UpdateSpriteMouseNotHovering();
        npcScript.descriptionPanel.SetActive(false);
        glassSprite.SetActive(false);
        NPCNameUI.gameObject.SetActive(false);
        foreach (GameObject answerUI in answerPanels)
            answerUI.SetActive(false);
        endPannel.SetActive(false);

        npcScript.gameObject.GetComponentInParent<Animator>().SetBool("IsTalking", false);
        player.GetComponent<Animator>().SetBool("IsTalking", false);
        dialogueHasNoAnswer = false;
        npcScript.m_playerScript.isInteracting = false;
        npcScript.m_playerScript.isTalking = false;

    }
}
