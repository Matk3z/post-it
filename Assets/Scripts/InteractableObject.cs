﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using LitJson;

public class InteractableObject : MonoBehaviour
{
    public bool hasBeenTriggered;
    public Player m_playerScript;
    public Vector2 m_playerDistance;
    public GameObject descriptionPanel;
    public PostIt postIt;
    public Sprite InteractableImage;
    public GameObject InteractableImageObject;

    private string objectDescription;
    private PostItBoard m_postItBoard;

    private void OnEnable()
    {
        m_playerScript = FindObjectOfType<Player>();
        m_postItBoard = FindObjectOfType<PostItBoard>();

        if (GetType() == typeof(InteractableObject))
        {
            GetDescription();
        }


    }

    // Update is called once per frame
    void Update()
    {
        m_playerDistance = new Vector2(m_playerScript.transform.position.x - transform.position.x, m_playerScript.transform.position.z - transform.position.z);

;
        DebugText.AddText($"{gameObject.name}", (m_playerDistance.magnitude < 1f).ToString());

        if(!m_postItBoard.isInBoard && m_playerDistance.magnitude < 1f && Input.GetKeyDown(ControllerInput.Instance.ActionButton) && !m_playerScript.isInteracting)
        {
            m_playerScript.isInteracting = true;
            hasBeenTriggered = true;

            if (!GameData.Instance.gameObjectTriggeredName.Contains(name))
            {
                print("OH YOW");
                GameData.Instance.gameObjectTriggeredName.Add(name);
            }

            if(postIt != null)
            {
                m_postItBoard.AddpostIt(postIt.postItName);
            }
            InteractObject();
        }
        else if(m_playerDistance.magnitude < 1f && m_playerScript.isInteracting && Input.GetKeyDown(ControllerInput.Instance.ActionButton) && !m_playerScript.isTalking)
        {

            m_playerScript.isInteracting = false;
            HideInteractUI(descriptionPanel);
        }


    }

    public void InteractObject()
    {

        StartCoroutine(RotateTo(10f));
        ShowInteractUI(descriptionPanel, objectDescription);
    }

    protected void ShowInteractUI(GameObject UIPanel, string text)
    {

        UIPanel.SetActive(true);
        Text descriptionText = UIPanel.GetComponentInChildren<Text>();
        InteractableImageObject.SetActive(true);
        InteractableImageObject.GetComponent<Image>().sprite = InteractableImage;

        descriptionText.text = text;

    }

    void HideInteractUI(GameObject UIPanel)
    {
        InteractableImageObject.SetActive(false);
        UIPanel.SetActive(false);

    }

    void GetDescription()
    {
        string objectName = gameObject.name;
        string descriptionString = File.ReadAllText(Application.dataPath + "/JsonData/Item_description.json", System.Text.Encoding.GetEncoding("iso-8859-1"));
        JsonData descriptionData = JsonMapper.ToObject(descriptionString);

        objectDescription = descriptionData[objectName].ToString();

    }

    IEnumerator RotateTo(float speed)
    {
        Rigidbody rb = m_playerScript.gameObject.GetComponent<Rigidbody>();
        Quaternion newRotation = Quaternion.LookRotation(-m_playerDistance.normalized);
        float yAngle = 0;

        while (Mathf.Abs(yAngle - newRotation.eulerAngles.y) > 1f)
        {
            yAngle = Mathf.Lerp(m_playerScript.transform.eulerAngles.y, newRotation.eulerAngles.y, Time.deltaTime * speed);
            rb.MoveRotation(Quaternion.Euler(0, yAngle, m_playerScript.transform.eulerAngles.z));

            yield return null;
        }

        print("rotation reached");

    }
}
