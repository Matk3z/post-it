﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PostItBoard : MonoBehaviour
{
    public PostIt[] postItScriptables;
    public List<GameObject> postItList;
    public GameObject postItBoardUI;
    public GameObject postItPrefab;
    public List<GameObject> linkedPostIt = new List<GameObject>();
    public GameObject ropePrefab;
    public GameObject postItAddUIPrefab;
    public GameObject postItUpdateUIPrefab;
    public GameObject canvas;
    public bool isMoving = false;
    public bool isLinking = false;
    public bool isInBoard = false;
    public bool isMovingAPostIt = false;

    private List<GameObject> ropes = new List<GameObject>();
    private bool continueCoroutine;
    private GameObject sourcePostIt;
    private GameObject targetPostIt;
    private IEnumerator coroutineFollowMouse;
    private IEnumerator coroutineFollowPostIt;

    public void Start()
    {
        foreach (string postItListName in GameData.Instance.PostItListName)
        {
            AddpostIt(postItListName);
        }

    }

    public void AddpostIt(string postItName)
    {

        foreach(PostIt postItScriptable in postItScriptables)
        {
            if (postItScriptable.postItName == postItName)
            {
                foreach (GameObject postItGameObject in postItList)
                    if (postItGameObject.GetComponent<PostItDisplay>().postIt.postItName == postItName)
                    {
                        print($"Post it with the name : {postItName} already exist");
                        return;
                    }


                GameObject postIt = Instantiate(postItPrefab, postItBoardUI.transform);
                postIt.GetComponent<PostItDisplay>().postIt = postItScriptable;
                postIt.name = $"Post it : {postItName}";
                postIt.GetComponent<PostItDisplay>().DisplayInfo();
                postIt.GetComponentInChildren<Button>().onClick.AddListener(delegate { LinkPostIt(postIt); });
                postIt.GetComponentInChildren<Button>().onClick.AddListener(delegate { StartRopeFollowingMouse(postIt); });
                postItList.Add(postIt);
                Instantiate(postItAddUIPrefab, canvas.transform);
                if(!GameData.Instance.PostItListName.Contains(postIt.GetComponent<PostItDisplay>().postIt.postItName))
                    GameData.Instance.PostItListName.Add(postIt.GetComponent<PostItDisplay>().postIt.postItName);

                return;
            }
        }
        print($"No Post it with the name : {postItName} exist");
    }

    public void UpdatePostIt(string name, string updatedString)
    {
        foreach (GameObject postItGameObject in postItList)
        {
            if(postItGameObject.GetComponent<PostItDisplay>().postIt.postItName == name)
            {
                print("foo");
                postItGameObject.GetComponent<PostItDisplay>().postIt.postItName = updatedString;
                postItGameObject.GetComponent<PostItDisplay>().DisplayInfo();
                Instantiate(postItUpdateUIPrefab, canvas.transform);
            }
        }
        print($"No Post it with the name : {name} exist");
    }

    public void AddAllPostIt()
    {
        foreach (PostIt postItScriptable in postItScriptables)
        {
            print(postItScriptable.postItName);
            AddpostIt(postItScriptable.postItName);
        }

    }


    public void StartRopeFollowingMouse(GameObject source)
    {
        if (isLinking || linkedPostIt.Count >= 3)
            return;

        if (linkedPostIt.Count > 0 && !linkedPostIt.Contains(source))
            return;

        ropes.Add(Instantiate(ropePrefab, postItBoardUI.transform));
        sourcePostIt = source;
        print("foo");
        coroutineFollowMouse = RopeFollowMouse(source);
        StartCoroutine(coroutineFollowMouse);
        isLinking = true;
    }

    public void DestroyLastRope()
    {

        print("stop rope following mouse");
        StopCoroutine(coroutineFollowMouse);

        GetComponent<GenerateIdCardScroll>().SuspectName = "";
        GetComponent<GenerateIdCardScroll>().GenerateIdCardScrollObject();

        if (linkedPostIt.Count == 0 )
        {
            Image pinImage = sourcePostIt.GetComponent<PostItDisplay>().pinGameObject.GetComponent<Image>();
            pinImage.color = new Color(pinImage.color.r, pinImage.color.g, pinImage.color.b, 0);
            Destroy(ropes.Last());
            ropes.Remove(ropes.Last());
            isLinking = false;
            return;
        }

        if (ropes.Last().name == "Rope(Clone)")
        {
            Destroy(ropes.Last());
            ropes.Remove(ropes.Last());
            isLinking = false;
            return;
        }

        string sourceAndTarget = ropes.Last().name.Split('=')[1];
        string targetName = sourceAndTarget.Split('/')[1].Trim();

        if(linkedPostIt.Count <= 2)
        {
            foreach(GameObject linkedPostItGameObject in linkedPostIt)
            {
                Image pinImage = linkedPostItGameObject.GetComponent<PostItDisplay>().pinGameObject.GetComponent<Image>();
                pinImage.color = new Color(pinImage.color.r, pinImage.color.g, pinImage.color.b, 0);
            }
            linkedPostIt = new List<GameObject>();
            Destroy(ropes.Last());
            ropes.Remove(ropes.Last());
            isLinking = false;
            return;
        }

        GameObject targetToRemove = new GameObject();

        foreach(GameObject linkedPostItGameObject in linkedPostIt)
        {
            print(linkedPostItGameObject.name);
            if(linkedPostItGameObject.name == targetName)
            {
                targetToRemove  = linkedPostItGameObject;
            }
        }
        Image targetPinImage = targetToRemove.GetComponent<PostItDisplay>().pinGameObject.GetComponent<Image>();
        targetPinImage.color = new Color(targetPinImage.color.r, targetPinImage.color.g, targetPinImage.color.b, 0);
        linkedPostIt.Remove(targetToRemove);
        Destroy(ropes.Last());
        ropes.Remove(ropes.Last());
        isLinking = false;
        return;
    }


    public void LinkPostIt(GameObject target)
    {

        if (!isLinking)
            return;
        if (sourcePostIt.name == target.name)
            return;

        targetPostIt = target;

        if (linkedPostIt.Contains(sourcePostIt) && linkedPostIt.Contains(targetPostIt))
        {
            print("Post it are already linked");
            DestroyLastRope();
            return;
        }


        coroutineFollowPostIt = RopeFollowPostIt(sourcePostIt, targetPostIt);
        ropes.Last().name = $"rope = {sourcePostIt.name} / {targetPostIt.name}";
        if (!linkedPostIt.Contains(sourcePostIt))
            linkedPostIt.Add(sourcePostIt);

        linkedPostIt.Add(targetPostIt);
        continueCoroutine = false;
        StartCoroutine(coroutineFollowPostIt);

        string[] suspect = new string[3];
        if(linkedPostIt.Count >= 3)
        {
               for(int i = 0; i < 3; i++)
            {
                suspect[i] = linkedPostIt[i].GetComponent<PostItDisplay>().postIt.name.Split('-')[0].Trim();
                print(suspect[i]);
            }
               if(suspect[0] == suspect[1] && suspect[1] == suspect[2])
            {
                GetComponent<GenerateIdCardScroll>().SuspectName = suspect[0];
                GetComponent<GenerateIdCardScroll>().GenerateIdCardScrollObject();
            }
        }
    }


    private IEnumerator RopeFollowMouse(GameObject source)
    {
        print("Start Rope follow mouse");
        continueCoroutine = true;
        GameObject rope = ropes.Last();
        RectTransform ropeTransform = rope.GetComponent<RectTransform>();
        RectTransform sourceTransform = source.GetComponent<RectTransform>();
        Vector3 targetPostItPosition;
        Vector3 offset = source.GetComponent<PostItDisplay>().pinGameObject.GetComponent<RectTransform>().localPosition;
        Image pinImage = source.GetComponent<PostItDisplay>().pinGameObject.GetComponent<Image>();
        pinImage.color = new Color(pinImage.color.r, pinImage.color.g, pinImage.color.b, 1);

        while (continueCoroutine)
        {
            targetPostItPosition   = Input.mousePosition - sourceTransform.position - offset;
            Vector3 ropePosition   = (sourceTransform.position + Input.mousePosition + offset) / 2;
            ropeTransform.position = ropePosition;
            ropeTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, targetPostItPosition.magnitude * 5f);
            ropeTransform.rotation = Quaternion.LookRotation(Input.mousePosition, Vector3.up);

            float angle = Vector3.Angle(targetPostItPosition, transform.up);

            Vector3 cross = Vector3.Cross(targetPostItPosition, transform.up);


            if(cross.z > 0)
            {
                angle = -angle;
            }

            ropeTransform.eulerAngles = new Vector3(0, 0, angle + 90f);

            yield return null;
        }
        pinImage.color = new Color(pinImage.color.r, pinImage.color.g, pinImage.color.b, 0);
        isLinking = false;
        print("Rope follow mouse end");
    }

    private IEnumerator RopeFollowPostIt(GameObject source, GameObject target)
    {
        print("Start Rope follow postIt");

        GameObject rope = ropes.Last();
        RectTransform ropeTransform   = rope.GetComponent<RectTransform>();
        RectTransform sourceTransform = source.GetComponent<RectTransform>();
        RectTransform targetTransform = target.GetComponent<RectTransform>();
        Vector3 targetPostItPosition;
        RectTransform sourcePinTransform = source.GetComponent<PostItDisplay>().pinGameObject.GetComponent<RectTransform>();
        RectTransform targetPinTransform = target.GetComponent<PostItDisplay>().pinGameObject.GetComponent<RectTransform>();
        Image targetPinImage = target.GetComponent<PostItDisplay>().pinGameObject.GetComponent<Image>();
        Image sourcePinImage = source.GetComponent<PostItDisplay>().pinGameObject.GetComponent<Image>();
        sourcePinImage.color = new Color(sourcePinImage.color.r, sourcePinImage.color.g, sourcePinImage.color.b, 1);
        targetPinImage.color = new Color(targetPinImage.color.r, targetPinImage.color.g, targetPinImage.color.b, 1);
        Vector3 sourceOffset = sourcePinTransform.localPosition;
        float sourceScale;
        Vector3 targetOffset = targetPinTransform.localPosition;
        float targetScale;
        float coroutineStarting = 0;

        while (true)
        {

            if (coroutineStarting < 2)
            {
                sourcePinImage.color = new Color(sourcePinImage.color.r, sourcePinImage.color.g, sourcePinImage.color.b, 1);
                targetPinImage.color = new Color(targetPinImage.color.r, targetPinImage.color.g, targetPinImage.color.b, 1);
                coroutineStarting++;
            }

            sourceScale = sourceTransform.localScale.x;
            targetScale = targetTransform.localScale.x;


            targetPostItPosition      = (sourceTransform.position + sourceOffset * sourceScale) - (targetTransform.position + targetOffset * targetScale);
            Vector3 ropeFinalPosition = (sourceTransform.position + targetTransform.position + (sourceOffset * sourceScale) + (targetOffset * targetScale)) / 2;
            ropeTransform.position    = ropeFinalPosition;
            ropeTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, targetPostItPosition.magnitude * 5f);
            ropeTransform.rotation    = Quaternion.LookRotation(targetTransform.position, Vector3.up);

            float finalAngle = Vector3.Angle(targetPostItPosition, transform.up);

            Vector3 finalCross = Vector3.Cross(targetPostItPosition, transform.up);


            if (finalCross.z > 0)
            {
                finalAngle = -finalAngle;
            }

            ropeTransform.eulerAngles = new Vector3(0, 0, finalAngle + 90f);
            yield return null;
        }
    }
}
