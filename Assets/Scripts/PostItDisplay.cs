﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PostItDisplay : MonoBehaviour
{
    
    public PostIt postIt;

    private Text description;
    private Text postItName;
    private GameManager gameManager;
    private IEnumerator coroutine;
    private bool isFollowingMouse = false;
    private Vector3 postItPosition;
    public GameObject pinGameObject;

    public float lastUsed;



    // Start is called before the first frame update
    public void DisplayInfo()
    {
        gameManager = FindObjectOfType<GameManager>();
        Text[] texts = GetComponentsInChildren<Text>();
        foreach (Text text in texts)
        {
            switch (text.gameObject.name)
            {
                case "Post it name":
                    postItName = text;
                    break;
                case "Post it description":
                    description = text;
                    break;
            }
        }
        postItName.text = postIt.postItName;
        GetComponent<RectTransform>().localPosition = postIt.position;
    }

    private void LateUpdate()
    {
        postItPosition = GetComponent<RectTransform>().position;
        if(!gameManager.GetComponent<PostItBoard>().isLinking  && Mathf.Abs(postItPosition.x - Input.mousePosition.x) + 70f < GetComponent<RectTransform>().rect.size.x && Mathf.Abs(postItPosition.y - Input.mousePosition.y) + 70f < GetComponent<RectTransform>().rect.size.y)
        {
            GetComponent<Animator>().SetBool("IsMouseHovering", true);
            if (Input.GetMouseButtonDown(0) && !isFollowingMouse && !gameManager.GetComponent<PostItBoard>().isMovingAPostIt)
            {
                StartMovingPostIt();
            }
        }
        else
        {
            GetComponent<Animator>().SetBool("IsMouseHovering", false);
        }
        if(gameManager.GetComponent<PostItBoard>().isInBoard && Input.GetMouseButtonUp(0) && isFollowingMouse)
        {
            StopMovingPostIt();
            gameManager.GetComponent<PostItBoard>().isMoving = false;
        }
    }

    public void StartMovingPostIt()
    {
        gameManager.GetComponent<PostItBoard>().isMovingAPostIt = true;
        if (!isFollowingMouse && Time.time - lastUsed > 0.01f)
        {
            Vector3 offset = postItPosition - Input.mousePosition;
            coroutine = MoveFromMouse(offset);
            StartCoroutine(coroutine);
            isFollowingMouse = true;
        }
    }

    public void StopMovingPostIt()
    {
        StopCoroutine(coroutine);
        isFollowingMouse = false;
        gameManager.GetComponent<PostItBoard>().isMovingAPostIt = false;
        lastUsed = Time.time;
    }


    IEnumerator MoveFromMouse(Vector3 offset)
    {
        while (true)
        {
            GetComponent<RectTransform>().position = Input.mousePosition + offset;
            yield return null;
        }
    }

}
