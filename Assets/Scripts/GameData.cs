﻿using System;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;
using UnityEngine;


public sealed class GameData : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static GameData instance { get; private set; }
    private static readonly object padlock = new object();

    GameData()
    {
    }

    public static GameData Instance
    {
        get
        {

            lock (padlock)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<GameData>();
                    if (instance == null)
                    {
                        GameObject container = new GameObject("GameData");
                        instance = container.AddComponent<GameData>();
                        DontDestroyOnLoad(container);
                    }

                }
                return instance;
            }
        }
    }

    #endregion


    // The name off all the post it that has been discovered
    public List<string> PostItListName = new List<string>();

    public List<string> gameObjectTriggeredName = new List<string>();

    public UserData.Parameter gameParameter = new UserData.Parameter(1, 1);

    [Serializable]
    public class UserData
    {

        public List<string> PostItListName = new List<string>();
        public List<string> gameObjectTriggeredName = new List<string>();

        [Serializable]
        public struct Parameter {

            public float MainVolume;
            public float MusicVolume;

            public Parameter(float musicVolume, float mainVolume)
            {
                MainVolume = mainVolume;
                MusicVolume = musicVolume;
            }

        }

        public Parameter gameParameter;

        public UserData(List<string> postItList, List<string> gameobjecttriggeredname, Parameter GameParameter)
        {
            PostItListName = postItList;
            gameObjectTriggeredName = gameobjecttriggeredname;
            gameParameter = GameParameter;
        }

    }

    public void SaveGame()
    {
        if(!File.Exists(Application.dataPath + "/gamedata.save"))
        {
            Debug.Log("Save doesn't exist creating one");
            File.Create(Application.dataPath + "/gamedata.save");
        }

        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Open(Application.dataPath + "/gamedata.save", FileMode.Open);
        UserData userData = new UserData(PostItListName, gameObjectTriggeredName, gameParameter);
        bf.Serialize(file, userData);
        file.Close();

        Debug.Log("Game Saved");
    }

    public void DeleteSaveFile()
    {
        File.Delete(Application.dataPath + "/gamedata.save");

        PostItListName = new List<string>();

}

public void LoadGame()
    {
        if(!File.Exists(Application.dataPath + "/gamedata.save"))
        {
            Debug.Log("Save file doesn't exist cannot load the game");
            return;
        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.dataPath + "/gamedata.save", FileMode.Open);

        UserData userData = (UserData)bf.Deserialize(file);
        file.Close();

        if(userData.PostItListName != null)
        {
            PostItListName = userData.PostItListName;
        }

        if(userData.gameObjectTriggeredName != null)
        {
            gameObjectTriggeredName = userData.gameObjectTriggeredName;
        }

        gameParameter = userData.gameParameter;

    }



}
