﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorFix : MonoBehaviour
{
    private void OnPreCull()
    {
        Camera camera = GetComponent<Camera>();

        camera.ResetWorldToCameraMatrix();
        camera.ResetProjectionMatrix();
        camera.projectionMatrix = camera.projectionMatrix * Matrix4x4.Scale(new Vector3(-1, 1, 1));
        
    }

    private void OnPreRender()
    {
        GL.invertCulling = true;
    }
    private void OnPostRender()
    {
        GL.invertCulling = false;
    }
}
