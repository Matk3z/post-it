﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class DebugText
{
    static Dictionary<string, string> _debuginfo = new Dictionary<string, string>();


    public static void AddText(string key, string value)
    {
        if (_debuginfo.ContainsKey(key) || _debuginfo.ContainsValue(value))
            _debuginfo[key] = value;
        else
            _debuginfo.Add(key, value);

    }

    public static void DeleteText()
    {
        _debuginfo = new Dictionary<string, string>();
    }
    public static string GetString()
    {

        string stringValue = "";
        foreach (KeyValuePair<string, string> entry in _debuginfo)
        {
            stringValue += $"{entry.Key} : {entry.Value}/ ";
        }
        return stringValue;
    }
}
