﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PostIt", menuName = "PostIt")]
public class PostIt : ScriptableObject
{
    public string postItName;
    public Vector3 position;

    public Sprite PostItImage;
}
