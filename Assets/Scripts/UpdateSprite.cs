﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UpdateSprite : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Sprite mouseHovering;
    public Sprite mouseNotHovering;


    public void OnPointerEnter(PointerEventData eventData)
    {
        GetComponent<Image>().sprite = mouseHovering;
        GetComponent<Image>().SetNativeSize();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().sprite = mouseNotHovering;
    }

    public void UpdateSpriteMouseNotHovering()
    {
        GetComponent<Image>().sprite = mouseNotHovering;
    }
}
