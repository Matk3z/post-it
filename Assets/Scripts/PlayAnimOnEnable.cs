﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimOnEnable : MonoBehaviour
{
    public AnimationClip animationToPlay;
    private void OnEnable()
    {
        GetComponent<Animator>().Play(animationToPlay.name);
    }
}