﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed;
    public float maxVelocity;
    public bool isInteracting;
    public GameObject gameManager;
    public GameObject camera;
    public bool isTalking;

    private float m_direction;
    private Rigidbody m_rb;




    // Start is called before the first frame update
    void Start()
    {
        m_rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DebugText.AddText("Player Velocity", m_rb.velocity.magnitude.ToString("N2"));
        if (UpdatePlayerDirectionInput() && !isInteracting && !gameManager.GetComponent<PostItBoard>().isInBoard)
        {

            Quaternion toRotation = Quaternion.Euler(0, camera.transform.rotation.eulerAngles.y + m_direction * 90, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, toRotation, Time.fixedDeltaTime * 25);
            if(m_rb.velocity.magnitude < maxVelocity)
            m_rb.AddForce(transform.forward * speed);

        }
        if (GetComponent<Rigidbody>().velocity.magnitude < 1)
        {
            GetComponent<Animator>().SetBool("IsMoving", false);
        }
        else
        {
            GetComponent<Animator>().SetBool("IsMoving", true);
        }
        
    }

    bool UpdatePlayerDirectionInput()
    {
        int directionx = 0;
        int directiony = 0;
        bool xPressed = false;
        bool yPressed = false;

        if (Input.GetKey(ControllerInput.Instance.BackWardButton))
        {
            directiony -= 2;
            yPressed = true;
        }
        if (Input.GetKey(ControllerInput.Instance.RightButton))
        {
            directionx += 1;
            directiony = 2;
            xPressed = true;
        }
        if (Input.GetKey(ControllerInput.Instance.LeftButton))
        {
            directionx -= 1;
            directiony = -2;
            xPressed = true;
        }
        if (Input.GetKey(ControllerInput.Instance.ForwardButton))
        {
            directiony = 0;
            yPressed = true;
        }

        if (!yPressed && !xPressed)
        {
            return false;
        }
        else if(xPressed && yPressed)
        {
            m_direction = (directionx + directiony) / 2f;
        }
        else if(!xPressed && yPressed)
        {
            m_direction = directiony;
        }
        else if(xPressed && !yPressed)
        {
            m_direction = directionx;
        }

        return true;
    }
}
